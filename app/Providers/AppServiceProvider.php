<?php

namespace App\Providers;

use App\Models\About;
use App\Models\ShopCart;
use App\Models\WishLists;
use App\Models\FunctionAvailable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    // public $count_shop_cart;
    public function boot()
    {
        View::composer('*', function ($view) {
            $about  = About::first();
            if (Auth::check()) {
                $about  = About::first();
                $count_shop_cart = ShopCart::where('creator_id',auth()->user()->id)->count('id');
                $count_wishLists = WishLists::where('creator_id',auth()->user()->id)->count('id');
                $function_available = FunctionAvailable::select('function_availables.*')
                    ->join('functions as f', 'f.id', '=', 'function_availables.function_id')
                    ->where('function_availables.role_id', auth()->user()->roles_id)
                    ->orderBy('f.id', 'ASC')->get();
                View::share(([
                    'count_wishLists' => $count_wishLists,
                    'count_shop_cart' => $count_shop_cart,
                    'about' => $about,
                    'function_available' => $function_available,
                ]));
            }
            View::share(([
                'about' => $about,
            ]));
            View::composer('*', function ($view) {
                $view->with('currentRoute', Route::currentRouteName());
            });
        });
    }
}
