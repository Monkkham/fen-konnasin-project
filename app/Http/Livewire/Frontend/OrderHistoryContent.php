<?php

namespace App\Http\Livewire\Frontend;

use App\Models\Sales;
use Livewire\Component;

class OrderHistoryContent extends Component
{
    public function render()
    {
        $data = Sales::withSum('sales_detail', 'subtotal')->where('customer_id', auth()->user()->id)->get();
        return view('livewire.frontend.order-history-content', compact('data'))->layout('layouts.frontend.style');
    }
}
