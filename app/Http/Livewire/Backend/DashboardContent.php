<?php

namespace App\Http\Livewire\Backend;

use App\Models\User;
use App\Models\Sales;
use App\Models\Orders;
use App\Models\Product;
use Livewire\Component;
use App\Models\IncomeExpend;
use Livewire\WithPagination;

class DashboardContent extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';
    public $sum_total_sale, $sum_total_order, $count_product, $count_order, $count_sale, $count_employee, $count_customer;
    public $start_date, $end_date, $type;
    public function mount()
    {
        $this->start_date = date('Y-m-d');
        $this->end_date = date('Y-m-d');
    }
    public function render()
    {
        $this->sum_total_order = Orders::select('total')->sum('total');
        $this->sum_total_sale = Sales::select('total')->sum('total');
        $this->count_product = Product::select('id')->count('id');

        $this->count_order = Orders::select('id')->count('id');
        $this->count_sale = Sales::select('id')->count('id');

        $this->count_employee = User::select('id')->whereIn('roles_id', [1, 2])->count('id');
        $this->count_customer = User::select('id')->where('roles_id', 4)->count('id');

        $sales = Sales::where('status', 1)->where('type_sale',2)->paginate(5);

        // report all expend income 
        $end = date('Y-m-d H:i:s', strtotime($this->end_date . '23:23:59'));
        // if ($this->start_date && $this->end_date) {
            $sales = Sales::get();
            $orders = Orders::get();

            $sum_money_income = IncomeExpend::where('type',1)->sum('total_price');
            $sum_money_expend = IncomeExpend::where('type',2)->sum('total_price');
        // } else {
        //     $sales = [];
        //     $orders = [];
        // }
        $sum_total_income = $sales->sum('total');
        $sum_total_expend = $orders->sum('total');

        return view('livewire.backend.dashboard-content', compact('sales','orders','sum_total_income','sum_total_expend','sum_money_income','sum_money_expend'))->layout('layouts.backend.style');
    }
}
