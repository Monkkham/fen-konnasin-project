<?php

namespace App\Http\Livewire\Backend\Sales;

use App\Models\User;
use App\Models\Sales;
use Livewire\Component;
use App\Models\SalesCart;
use App\Models\SalesDetail;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\DB;

class ListOrderShopContent extends Component
{
    use WithFileUploads;
    use WithPagination;
    public $ID, $onepay;
    protected $paginationTheme = 'bootstrap';
    public $search_product, $search, $start_date, $end_date, $status, $branchs, $sum_subtotal, $qty, $customer_id, $customer_data, $type = 2, $note, $product_type_id;
    public function mount()
    {
        $this->start_date = date('Y-m-d');
        $this->end_date = date('Y-m-d');
    }
    public function render()
    {
        $end = date('Y-m-d H:i:s', strtotime($this->end_date . '23:23:59'));
        $data = Sales::withSum('sales_detail', 'subtotal')->where(function ($q) {
            $q->where('type', 'like', '%' . $this->search . '%')
                ->orwhere('code', 'like', '%' . $this->search . '%');
        })->where('type_sale', 2)->get();
        if ($this->start_date && $this->end_date) {
            $data = $data->whereBetween('created_at', [$this->start_date, $end]);
        }
        if ($this->status) {
            $data = $data->where('status', $this->status);
        }
        if (!empty($data)) {
            $data = $data;
        } else {
            $data = [];
        }
        $customers = User::all();
        $sale_cart = SalesCart::all();
        $count_cart = SalesCart::count();
        return view('livewire.backend.sales.list-order-shop-content', compact('data','customers','sale_cart','count_cart'))->layout('layouts.backend.style');
    }
    public function resetField()
    {
        $this->ID = '';
        $this->code = '';
        $this->note = '';
    }
    public function showOnepay($ids)
    {
        $this->ID = $ids;
        $data = Sales::find($ids);
        $this->onepay = $data->onepay;
        $this->type = $data->type;
        $this->dispatchBrowserEvent('show-modal-onepay');
    }
    public function ConfirmOnepay()
    {
        $data = Sales::find($this->ID);
        $data->type = 3;
        $data->save();
        $this->dispatchBrowserEvent('hide-modal-delete');
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ສຳເລັດເເລ້ວ!',
            'icon' => 'success',
        ]);
        return redirect(route('backend.ListOrderShop'));
    }
    public $salesDetail = [], $stock = [], $code;
    public function ShowUpdate($ids)
    {
        $this->dispatchBrowserEvent('show-modal-update-item');
        $sales = Sales::find($ids);
        $this->ID = $sales->id;
        $this->code = $sales->code;
        $this->salesDetail = SalesDetail::where('sales_id', $this->ID)->get();
        $this->stock = $this->salesDetail->pluck('stock');
    }

    public function UpdateStock($id)
    {
        $salesDetail = SalesDetail::find($id);
        $salesDetail->stock = $this->stock[$id];
        $salesDetail->subtotal = $salesDetail->sell_price * $this->stock[$id];
        $salesDetail->save();
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ແກ້ໄຂຈຳນວນສຳເລັດ!',
            'icon' => 'success',
        ]);
    }

    public function Remove_Item($id)
    {
        $SalesDetail = SalesDetail::find($id);
        $SalesDetail->delete();
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ລຶບຂໍ້ມູນສຳເລັດ!',
            'icon' => 'success',
        ]);
    }
    public function showDestory($ids)
    {
        $this->ID = $ids;
        $data = Sales::find($ids);
        // $this->name = $data->name;
        $this->dispatchBrowserEvent('show-modal-delete');
    }

    public function Destroy()
    {
        $sales = Sales::find($this->ID);

        if (!$sales) {
            return;
        }

        $salesDetails = SalesDetail::where('sales_id', $this->ID)->get();
        foreach ($salesDetails as $salesDetail) {
            $salesDetail->delete();
        }
        // $salesLogs = SalesLogs::where('sales_id', $this->ID)->get();
        // foreach ($salesLogs as $salesLog) {
        //     $salesLog->delete();
        // }
        $sales->delete();
        $this->resetField();

        $this->dispatchBrowserEvent('hide-modal-delete');
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ສຳເລັດເເລ້ວ!',
            'icon' => 'success',
        ]);
        return redirect(route('backend.ListOrderShop'));
    }
    public $SalesDetail = [], $sum_SalesDetail_subtotal, $sum_SalesDetail_stock;
    public function ShowBill($id)
    {
        $this->resetField();
        $this->dispatchBrowserEvent('show-modal-bill');
        $sales = Sales::find($id);
        $this->ID = $sales->id;
        $this->customer_data = $sales->customer;
        $this->code = $sales->code;
        $this->sum_SalesDetail_subtotal = SalesDetail::select('subtotal')->where('sales_id', $this->ID)->sum('subtotal');
        $this->sum_SalesDetail_stock = SalesDetail::select('stock')->where('sales_id', $this->ID)->sum('stock');
        $this->SalesDetail = SalesDetail::where('sales_id', $this->ID)->get();
    }

    public $shipping_start, $shipping_end;
    public function ShowShipping($ids)
    {
        $this->resetField();
        $data = Sales::find($ids);
        $this->ID = $data->id;
        // $this->shipping_start = $data->shipping_start;
        // $this->shipping_end = $data->shipping_end;
        $this->dispatchBrowserEvent('show-modal-add-edit');
    }
    public function Shipping()
    {
        $this->validate([
            'shipping_start' => 'required',
            'shipping_end' => 'required',
        ], [
            'shipping_start.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
            'shipping_end.required' => 'ກະລຸນາປ້ອນຂໍ້ມູນກ່ອນ!',
        ]);
        try {
            DB::beginTransaction();
            $data = Sales::find($this->ID);
            $data->shipping_start = $this->shipping_start;
            $data->shipping_end = $this->shipping_end;
            $data->status = 2;
            $data->update();
            DB::commit();
            $this->resetField();
            $this->dispatchBrowserEvent('hide-modal-add-edit');
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ກຳລັງຂົນສົ່ງສີນຄ້າ!',
                'icon' => 'success',
            ]);
            return redirect(route('backend.ListOrderShop'));
        } catch (\Exception $ex) {
            DB::rollBack();
            $this->dispatchBrowserEvent('swal', [
                'title' => 'ມີບາງຢ່າງຜິດພາດ!',
                'icon' => 'error',
            ]);
        }
    }
    public function ConfitmSuccess($ids)
    {
        $this->resetField();
        $data = Sales::find($ids);
        $this->ID = $data->id;
        $data->status = 3;
        $data->update();
        $this->dispatchBrowserEvent('swal', [
            'title' => 'ສົ່ງສຳເລັດເເລ້ວ!',
            'icon' => 'success',
        ]);
        return redirect(route('backend.ListOrderShop'));
        $this->dispatchBrowserEvent('show-modal-add-edit');
    }
}
