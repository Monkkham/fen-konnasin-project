<div wire:ignore.self class="modal fabe" id="modal-sales">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h5 class="modal-title"><i class="fas fa-file-alt"></i> ລາຍລະອຽດການຂາຍ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form-horizontal">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="form-group clearfix">
                                            <div class="icheck-success d-inline">
                                                <input type="radio" id="radioPrimary1" value="1" wire:model="type">
                                                <label for="radioPrimary1">ເງິນສົດ
                                                </label>
                                            </div>
                                            <div class="icheck-danger d-inline">
                                                <input type="radio" id="radioPrimary2" value="2" wire:model="type"
                                                    checked>
                                                <label for="radioPrimary2">ເງິນໂອນ
                                                </label>
                                            </div>
                                        </div>
                                        @error('type')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" wire:ignore>
                                        <select wire:model='customer_id' id="customer_id" class="form-control">
                                            <option value="" selected>----- ເລືອກ-ລູກຄ້າ -----</option>
                                            @foreach ($customers as $item)
                                                <option value="{{ $item->id }}">
                                                    {{ $item->name_lastname }} ໂທ: {{ $item->phone }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('customer_id')
                                            <span style="color: red" class="error">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class=" table">
                                <table class="table table-striped">
                                    <thead>
                                        @if ($customer_data)
                                            @if (!empty($customer_data))
                                                <tr>
                                                    <th class="bg-light">ຊື່ ນາມສະກຸນ:</th>
                                                    <th>{{ $customer_data->name_lastname }}
                                                        {{ $customer_data->name_lastname }}</th>
                                                </tr>
                                                <tr>
                                                    <th class="bg-light">ເພດ:</th>
                                                    <th>
                                                        @if ($customer_data->gender == 1)
                                                            <span>ຍິງ</span>
                                                        @elseif($customer_data->gender == 2)
                                                            <span>ຊາຍ</span>
                                                            @else
                                                            <span>ອື່ນໆ</span>
                                                        @endif
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th class="bg-light">ເບີໂທ:</th>
                                                    <th>{{ $customer_data->phone }}</th>
                                                </tr>
                                                <tr>
                                                    <th class="bg-light">ອີເມວ:</th>
                                                    <th>{{ $customer_data->email }}</th>
                                                </tr>
                                                <tr>
                                                    <th class="bg-light">ທີ່ຢູ່:</th>
                                                    <th>
                                                    @if (!empty($customer_data->province))
                                                            {{ $customer_data->village->name_la ?? '' }},
                                                            {{ $customer_data->district->name_la ?? '' }},
                                                            {{ $customer_data->province->name_la ?? '' }}
                                                            @else
                                                            -
                                                    @endif
                                                </th>
                                                </tr>
                                            @endif
                                        @endif
                                    </thead>
                                </table>
                            </div>
                    </div>
                    <div class="col-md-12">
                        <div class="invoice p-3 mb-2" id="forprint">
                            <!-- Table row -->
                            <div class="row">
                                <div class="col-md-12 table-responsive">
                                    <table class="table table-striped text-center">
                                        <thead>
                                            <tr class="bg-info">
                                                <th>ລຳດັບ</th>
                                                <th>ສິນຄ້າ</th>
                                                <th>ລາຄາ</th>
                                                <th>ຈຳນວນ</th>
                                                <th>ເປັນເງິນ</th>
                                            </tr>
                                        </thead>
                                        @php
                                            $num = 1;
                                        @endphp
                                        <tbody>
                                            @foreach ($sale_cart as $item)
                                            <tr>
                                                <td>{{ $num++ }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td><small>{{ number_format($item->price) }} ₭</small></td>
                                                <td>
                                                    {{ $item->qty }}
                                                </td>
                                                <td>{{ number_format($item->subtotal) }} ₭</td>
    
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <th colspan="4" class="text-bold text-right h5">ລວມຍອດ</th>
                                            <th colspan="2" class="text-bold h5">{{ number_format($this->sum_subtotal) }} ₭</th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i
                        class="fas fa-times-circle"></i> ຍົກເລີກ</button>
                <div wire:loading wire:target="PlaceSales">
                    <div class="spinner-border text-primary" role="status">
                        <span class="sr-only">ກຳລັງປະມວນຜົນ...</span>
                    </div>
                </div>
                @if($count_cart > 0)
                <button wire:click='PlaceSales' type="button" class="btn btn-success"><i
                    class="fas fa-check-circle"></i> ບັນທຶກການຂາຍ</button>
                    @else
                    <button disabled wire:click='PlaceSales' type="button" class="btn btn-success"><i
                        class="fas fa-check-circle"></i> ບັນທຶກການຂາຍ</button>
                @endif
            </div>
        </div>
    </div>
</div>

{{-- \\\\\\\\\\\\\\\\\\\\\\\ show bill  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ --}}
<div class="modal fade" id="modal-bill" wire:ignore.self>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fas fa-file-alt"></i> ລາຍລະອຽດບິນ: {{ $this->code }}
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <table>
                            <tr>
                                <th>
                                    <span>
                                        @if (!empty($about))
                                            <i class="fas fa-store-alt"></i> {{ $about->name_la }}
                                        @endif
                                    </span>
                                    <br>
                                    <span>
                                        @if (!empty($about))
                                            ໂທ: {{ $about->phone }}
                                        @endif
                                    </span><br>
                                    <span>
                                        @if (!empty($about))
                                            {{ $about->address }}
                                        @endif
                                    </span>
                                </th>

                            </tr>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table>
                            <tr>
                                <th>
                                    <span><i class="fas fa-user"></i> ລູກຄ້າ</span><br>
                                    <span>
                                        @if (!empty($customer_data))
                                            {{ $customer_data->name_lastname }}
                                        @endif
                                    </span>
                                    <br>
                                    <span>
                                        @if (!empty($customer_data))
                                            ໂທ: {{ $customer_data->phone }}
                                        @endif
                                    </span><br>
                                    <span>
                                        @if (!empty($customer_data))
                                            {{ $customer_data->address }}
                                        @endif
                                    </span>
                                </th>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="right_content">
                    <div class="row text-center pt-3">
                        <input type="hidden" wire:model="ID">
                        <div class="col-md-12">
                            <h4><b>ໃບບິນສັ່ງຊື້</b></h4>
                        </div>
                    </div>
                    <table class="table table-hover text-center responsive">
                        <thead class="bg-light text-center">
                            <tr>
                                <th>ລຳດັບ</th>
                                <th>ສິນຄ້າ</th>
                                <th>ລາຄາ</th>
                                <th>ຈຳນວນ</th>
                                <th>ເປັນເງິນ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $num = 1;
                            @endphp
                            @foreach ($SalesDetail as $item)
                                <tr class="text-center">
                                    <td>{{ $num++ }}</td>
                                    <td>
                                        @if (!empty($item->product))
                                            {{ $item->product->name }}
                                        @endif
                                    </td>
                                    <td>
                                        {{ number_format($item->buy_price) }} ₭
                                    </td>
                                    <td>
                                        x {{ $item->stock }}
                                    </td>
                                    <td>
                                        {{ number_format($item->subtotal) }} ₭
                                    </td>
                                </tr>
                            @endforeach
                            <tr class="text-bold bg-light">
                                <td colspan="3">ຍອດລວມ</td>
                                <td>x {{ number_format($this->sum_SalesDetail_stock) }}</td>
                                <td>{{ number_format($this->sum_SalesDetail_subtotal) }} ₭</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-secondary fas fa-times-circle" data-dismiss="modal">
                    ປິດ</button>
                <button id="print" type="button" class="btn btn-success"> <i class="fas fa-print"></i>
                </button>
            </div>
        </div>
    </div>
</div>

{{-- \\\\\\\\\\\\\\\\\\\\\\\ Edit order item \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ --}}
<div class="modal fade" id="modal-update-item" wire:ignore.self>
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fas fa-cart-plus"></i> ແກ້ໄຂໃບບິນ {{ $this->code }}
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row text-center">
                    <input type="hidden" wire:model="ID">
                    <div class="col-md-12">
                        <h4>ລາຍການສັ່ງຊື້</h4>
                    </div>
                </div>
                <table class="table table-hover text-center responsive">
                    <thead class="bg-light text-center">
                        <tr>
                            <th>ລຳດັບ</th>
                            <th>ສິນຄ້າ</th>
                            <th>ຈຳນວນ</th>
                            <th>ລາຄາ</th>
                            <th>ເປັນເງິນ</th>
                            <th>ຈັດການ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $num = 1;
                        @endphp
                        @foreach ($salesDetail as $item)
                            <tr class="text-center">
                                <td>{{ $num++ }}</td>
                                <td>
                                    @if (!empty($item->product))
                                        {{ $item->product->name }}
                                    @endif
                                </td>
                                <td>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input wire:model="stock.{{ $item->id }}" placeholder="0.00"
                                                    style="width: 80px" value="{{ $item->stock }}" min="1"
                                                    type="number"
                                                    class="form-control text-center money @error('stock.' . $item->id) is-invalid @enderror"
                                                    wire:change="UpdateStock({{ $item->id }})">
                                                @error('stock.' . $item->id)
                                                    <span style="color: #ff0000" class="error">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6 text-left">
                                            <div class="form-group">
                                                {{ $item->stock }}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    {{ number_format($item->sell_price) }} ₭
                                </td>
                                <td>
                                    {{ number_format($item->subtotal) }} ₭
                                </td>
                                <td>
                                    <button wire:click="Remove_Item({{ $item->id }})"
                                        class="btn btn-danger btn-sm"><i class="fas fa-times-circle"></i>
                                    </button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div wire:ignore class="modal fabe" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title"><i class="fa fa-trash"> </i> ລຶບອອກ</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 class="text-center">ທ່ານຕ້ອງການລຶບຂໍ້ມູນນີ້ອອກບໍ່?</h3>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary" data-dismiss="modal">ຍົກເລີກ</button>
                <button wire:click="Destroy({{ $ID }})" type="button" class="btn btn-success"><i
                        class="fa fa-trash"></i> ລຶບອອກ</button>
            </div>
        </div>
    </div>
</div>


{{-- ================ຈັດສົ່ງສິນຄ້າ================= --}}
<div wire:ignore.self class="modal fade" id="modal-add-edit">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header bg-light">
                <h4 class="modal-title"><i class="fas fa-truck-moving text-warning"></i> ຈັດສົ່ງສິນຄ້າ</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" wire:model="hiddenId">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for=""> ຜູ້ສົ່ງຕົ້ນທາງ</label>
                            <input type="text" class="form-control @error('shipping_start') is-invalid @enderror"
                                wire:model="shipping_start" placeholder="ປ້ອນຂໍ້ມູນ" />
                            @error('shipping_start')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for=""> ຜູ້ຮັບປາຍທາງ</label>
                            <input type="text" class="form-control @error('shipping_end') is-invalid @enderror"
                                wire:model="shipping_end" placeholder="ປ້ອນຂໍ້ມູນ" />
                            @error('shipping_end')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i
                        class="fa fa-times-circle"></i> ປິດ</button>

                <button wire:click="Shipping({{ $this->ID }})" type="button" class="btn btn-success"><i
                        class="fas fa-truck-moving"></i>
                    ຍືນຍັນຈັດສົ່ງ</button>
            </div>
        </div>
    </div>
</div>


<div wire:ignore.self class="modal fabe" id="modal-onepay">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title"><i class="fas fa-credit-card"> </i> ຫຼັກຖານການໂອນ</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <img src="{{ asset($this->onepay) }}" alt="" width="100%" height="400px">
            </div>
            @if (!($this->type == 3))
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">ປິດ</button>
                    <button type="button" wire:click='ConfirmOnepay({{ $this->ID }})'
                        class="btn btn-success btn-sm" data-dismiss="modal"><i class="fas fa-check-circle"></i>
                        ຍືນຍັນ</button>
                </div>
            @endif
        </div>
    </div>
</div>