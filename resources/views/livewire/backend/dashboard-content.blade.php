<div wire:poll>
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h5><i class="nav-icon fas fa-tachometer-alt"></i> ໜ້າຫຼັກ</h5>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('backend.dashboard') }}">ໜ້າຫຼັກ</a></li>
                        <li class="breadcrumb-item active">ໜ້າຫຼັກ</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- Main content -->
    @foreach ($function_available as $item1)
        @if ($item1->function->name == 'action_1')
            <section class="content">
                <div class="container-fluid">
                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-4 col-6">
                            <!-- small box -->
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h5>{{ number_format($this->sum_total_order) }} ₭</h5>
                                    <p>ລວມຍອດຊື້</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="{{ route('backend.order') }}" class="small-box-footer">ເບິ່ງລາຍລະອຽດ <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-4 col-6">
                            <!-- small box -->
                            <div class="small-box bg-warning">
                                <div class="inner">
                                    <h5>{{ number_format($this->sum_total_sale) }} ₭</h5>
                                    <p>ລວມຍອດຂາຍ</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="{{ route('backend.sale') }}" class="small-box-footer">ເບິ່ງລາຍລະອຽດ <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-4 col-6">
                            <!-- small box -->
                            <div class="small-box bg-success">
                                <div class="inner">
                                    <h5>{{ number_format($this->count_product) }} ລາຍການ</h5>
                                    <p>ສິນຄ້າທັງຫມົດ</p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-stats-bars"></i>
                                </div>
                                <a href="{{ route('backend.product') }}" class="small-box-footer">ເບິ່ງລາຍລະອຽດ <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>

                        <!-- ./col -->
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-info"><i class="fas fa-cart-arrow-down"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">ຈຳນວນສັ່ງຊື້ນຳຜູ້ສະຫນອງ</span>
                                    <span class="info-box-number">{{ $this->count_order }} <small> ລາຍການ</small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-warning"><i class="fas fa-cart-plus"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">ຈຳນວນຂາຍໃຫ້ລູກຄ້າ</span>
                                    <span class="info-box-number">{{ $this->count_sale }} <small> ລາຍການ</small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-success"><i class="fa fa-users"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">ພະນັກງານທັງຫມົກ</span>
                                    <span class="info-box-number">{{ $this->count_employee }} <small> ຄົນ</small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3 col-sm-6 col-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-danger"><i class="fa fa-users"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">ລູກຄ້າທັງຫມົດ</span>
                                    <span class="info-box-number">{{ $this->count_customer }} <small>
                                            ຄົນ</small></span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                    </div>
                    {{-- <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered responsive">
                                    <thead>
                                        <tr class="text-center bg-info text-md">
                                            <th colspan="8"><i class="fas fa-cart-plus"></i>
                                                ລາຍການສັ່ງຊື້ຜ່ານເວບໄຊລ່າສຸດ
                                            </th>
                                        </tr>
                                        <tr class="text-center bg-light">
                                            <th>ລຳດັບ</th>
                                            <th>ວັນທີ</th>
                                            <th>ລະຫັດ</th>
                                            <th>ລູກຄ້າ</th>
                                            <th>ເປັນເງິນ</th>
                                            <th>ສະຖານະ</th>
                                            <th>ຈັດການ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $stt = 1;    @endphp
                                        @foreach ($sales as $item)
                                            <tr>
                                                <td class="text-center">{{ $stt++ }}</td>
                                                <td class="text-center">
                                                    {{ date('d/m/Y', strtotime($item->created_at)) }}</td>
                                                <td class="text-center">{{ $item->code }}</td>
                                                <td class="text-center">
                                                    @if (!empty($item->customer))
                                                        {{ $item->customer->name_lastname }} <br><i
                                                            class="fas fa-phone-alt"></i> {{ $item->customer->phone }}
                                                    @endif
                                                </td>
                                                <td class="text-center">{{ number_format($item->total) }} ₭</td>
                                                <td class="text-center">
                                                    @if ($item->status == 1)
                                                        <span class="text-success"><b> <i class="fas fa-plus"></i>
                                                                ໃຫມ່</b></span>
                                                    @elseif ($item->status == 2)
                                                        <span class="text-warning">{{ __('lang.tran_pay') }}</span>
                                                    @endif
                                                </td>
                                                <td class="text-center">
                                                    <a href="{{ route('backend.ListOrderShop') }}"
                                                        class="btn btn-info btn-sm"><i class="fas fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="float-right">
                                    {{ $sales->links() }}
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered" id="table-excel">
                                <thead>
                                    <tr class="text-center bg-light text-md">
                                        <th colspan="8"><i class="fas fa-cart-plus"></i>
                                            ສະຫຼຸບລວມ
                                        </th>
                                    </tr>
                                    <tr class="text-center h6">
                                        <th class="bg-success text-success">
                                            ລວມລາຍຮັບ</th>
                                        <th class="bg-danger text-danger">
                                            ລວມລາຍຈ່າຍ</th>
                                        <th class="bg-warning text-warning">ກຳໄລ/ຂາດທຶນ
                                        </th>
                                    </tr>
                                    <tr class="text-center h6">
                                        <td class="text-bold text-success">
                                            {{ number_format($sum_total_income + $sum_money_income) }}
                                            ₭</td>
                                        <td class="text-bold text-danger">
                                            {{ number_format($sum_total_expend + $sum_money_expend) }}
                                            ₭</td>
                                        <td class="text-bold text-warning">
                                            {{ number_format(($sum_total_income + $sum_money_income) - ($sum_total_expend + $sum_money_expend)) }}

                                            ₭</td>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
        @endif
    @endforeach
    <!-- /.content -->

</div>
