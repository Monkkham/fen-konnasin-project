<div>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="#">ໜ້າຫຼັກ</a>
                    <span class="breadcrumb-item active">ຕິດຕໍ່</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->


    <!-- Contact Start -->
    <div class="container-fluid">
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">ຕິດຕໍ່ພວກເຮົາ</span></h2>
        <div class="row px-xl-5">
            <div class="col-lg-12 mb-5">
                <div class="contact-form bg-light p-30">
                    <div id="success"></div>
                    <form name="sentMessage" id="contactForm" novalidate="novalidate">
                        <div class="control-group">
                            <input wire:model='name' type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="ຊື່"
                                required="required" data-validation-required-message="ກະລຸນາປ້ອນ ຊື່" />
                                @error('name')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="control-group">
                            <input wire:model='phone' type="number" class="form-control @error('phone') is-invalid @enderror" id="phone" placeholder="ເບີໂທ"
                                required="required" data-validation-required-message="ກະລຸນາປ້ອນ ເບີໂທ" />
                                @error('phone')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="control-group">
                            <input wire:model='subject' type="text" class="form-control @error('subject') is-invalid @enderror" id="subject" placeholder="ຫົວຂໍ້"
                                required="required" data-validation-required-message="ກະລຸນາປ້ອນ ຫົວຂໍ້" />
                                @error('subject')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="control-group">
                            <textarea wire:model='message' class="form-control @error('subject') is-invalid @enderror" rows="8" id="message" placeholder="ຄຳອະທິບາຍ"
                                required="required"
                                data-validation-required-message="ກະລຸນາປ້ອນ ຄຳອະທິບາຍ"></textarea>
                                @error('subject')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                            <p class="help-block text-danger"></p>
                        </div>
                        <div>
                            <button class="btn btn-primary py-2 px-4" type="button" wire:click='SendMessage' id="sendMessageButton"> ສົ່ງຂໍ້ຄວາມ</button>
                        </div>
                    </form>
                </div>
            </div>
            {{-- <div class="col-lg-5 mb-5">
                <div class="bg-light p-30 mb-30">
                    <iframe style="width: 100%; height: 250px;"
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3001156.4288297426!2d-78.01371936852176!3d42.72876761954724!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4ccc4bf0f123a5a9%3A0xddcfc6c1de189567!2sNew%20York%2C%20USA!5e0!3m2!1sen!2sbd!4v1603794290143!5m2!1sen!2sbd"
                    frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
                <div class="bg-light p-30 mb-3">
                    <p class="mb-2"><i class="fa fa-map-marker-alt text-primary mr-3"></i>
                    @if(!empty($about))
                        {{ $about->address }}
                    @endif
                    </p>
                    <p class="mb-2"><i class="fa fa-envelope text-primary mr-3"></i>
                        @if(!empty($about))
                        {{ $about->email }}
                    @endif</p>
                    <p class="mb-2"><i class="fa fa-phone-alt text-primary mr-3"></i>
                        @if(!empty($about))
                        {{ $about->phone }}
                    @endif
                    </p>
                </div>
            </div> --}}
        </div>
    </div>
    <!-- Contact End -->
</div>
