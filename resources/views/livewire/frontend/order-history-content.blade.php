<div>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="#">ໜ້າຫຼັກ</a>
                    <a class="breadcrumb-item text-dark" href="#">ຮ້ານຄ້າ</a>
                    <span class="breadcrumb-item active">ປະຫວັດສັ່ງຊື້</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
    <!-- Cart Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-lg-12 table-responsive mb-5">
                <table class="table table-light table-borderless table-hover text-center mb-0">
                    <thead class="text-dark" style="background-color: {{ !empty($about->f_sidebar_color) ? $about->f_sidebar_color : '' }}">
                        <tr>
                            <th>ລໍາດັບ</th>
                            <th>ວັນເວລາ</th>
                            <th class="text-left">ລາຍລະອຽດສິນຄ້່າ</th>
                            <th>ລວມເງິນ</th>
                            <th>ສະຖານະ</th>
                        </tr>
                    </thead>
                    @php
                        $num = 1;
                    @endphp
                    <tbody class="align-middle">
                        @if (count($data) > 0)
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{ $num++ }}</td>
                                    <td>{{ date('d/m/Y', strtotime($item->created_at)) }} <br>
                                        {{ date('H:i:s', strtotime($item->created_at)) }}</td>
                                    <td class="text-left">
                                        @php
                                        $counts = 1;
                                    @endphp
                                        @foreach ($item->sales_detail as $items)
                                            @if (!empty($items->product))
                                                <small>{{ $counts++ }}. {{ $items->product->name }} {{ $items->product->sell_price }} <br></small>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        <b>{{ number_format($item->sales_detail_sum_subtotal) }} ₭</b>
                                    </td>
                                    <td>
                                        @if ($item->type == 1)
                                            <p class="text-success"><i class="fas fa-hand-holding-usd"></i>
                                                ເງິນສົດ</p>
                                        @elseif($item->type == 2)
                                        <p class="text-danger"><i class="fas fa-credit-card"></i>
                                            ເງິນໂອນ</p>
                                        @elseif($item->type == 3)
                                        <p class="text-danger"><i class="fas  fa-credit-card"></i>
                                            ເງິນໂອນ</p>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($item->status == 1)
                                            <span class="text-warning"><i class="fas fa-search"></i>
                                                ລໍຖ້າກວດສອບ</span>
                                        @elseif($item->status == 2)
                                            <span class="text-warning"><i class="fas fa-truck-moving"></i>
                                                ກຳລັງສົ່ງເດີເຈົ້າ</span>
                                        @elseif($item->status == 3)
                                            <span class="text-success"><i class="fas fa-check-circle"></i>
                                                ໄດ້ຮັບເເລ້ວ</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">
                                    <span class="text-danger"><i class="fas fa-box-open"></i> ບໍ່ມີສິນຄ້າໃນລາຍການທີ່ມັກ
                                    </span><a href="{{ route('frontend.shop') }}"> ໄປທີ່ຮ້ານຄ້າ <i
                                            class="fas fa-arrow-right"></i></a>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Cart End -->
</div>
