<div>
    <!-- Signup Start -->
    <div class="container-fluid">
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3"><i
                    class="fas fa-user-edit"></i> ລົງທະຽນໃຫມ່</span></h2>
        <div class="row px-xl-5 justify-content-center">
            <div class="col-lg-8 mb-5">
                <div class="contact-form bg-light p-30">
                    <div id="success"></div>
                    <form name="sentMessage" id="contactForm" novalidate="novalidate">
                        <div class="control-group">
                            <label>ຊື່ ເເລະ ນາມສະກຸນ</label>
                            <input type="text" wire:model='name_lastname'
                                class="form-control @error('name_lastname') is-invalid @enderror" id="name_lastname"
                                placeholder="ຊື່ ເເລະ ນາມສະກຸນ" required="required"
                                data-validation-required-message="Please enter your name" />
                            @error('name_lastname')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="control-group">
                            <label>ເບີໂທ</label>
                            <input type="number" wire:model="phone"
                                class="form-control @error('phone') is-invalid @enderror" id="number" min="1"
                                placeholder="ເບີໂທ 8 ຕົວເລກ" required="required"
                                data-validation-required-message="Please enter your phone" />
                            @error('phone')
                                <span style="color: red" class="error">{{ $message }}</span>
                            @enderror
                            <p class="help-block text-danger"></p>
                        </div>
                        <div class="row">
                            <div class="col-md-6 control-group">
                            <label>ລະຫັດຜ່ານ</label>
                                <input type="password" wire:model=password
                                    class="form-control @error('password') is-invalid @enderror" id="subject"
                                    placeholder="ລະຫັດຜ່ານ" required="required"
                                    data-validation-required-message="Please enter a subject" />
                                @error('password')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="col-md-6 control-group">
                            <label>ຍືນຍັນລະຫັດຜ່ານ</label>
                                <input type="password" wire:model='confirmPassword'
                                    class="form-control @error('confirmPassword') is-invalid @enderror" id="subject"
                                    placeholder="ຍືນຍັນລະຫັດຜ່ານ" required="required"
                                    data-validation-required-message="Please enter a subject" />
                                @error('confirmPassword')
                                    <span style="color: red" class="error">{{ $message }}</span>
                                @enderror
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="d-flex justify-content-center">
                            <button wire:click='SignUp' class="btn btn-primary py-2 px-4" type="button">
                                <i class="fas fa-pen"></i> ລົງທະບຽນ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact End -->
</div>
