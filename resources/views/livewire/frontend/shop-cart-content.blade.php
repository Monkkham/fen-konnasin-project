<div>
    <!-- Breadcrumb Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-dark" href="#">ໜ້າຫຼັກ</a>
                    <a class="breadcrumb-item text-dark" href="#">ຮ້ານຄ້າ</a>
                    <span class="breadcrumb-item active">ກະຕ່າ</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
    <!-- Cart Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-lg-8 table-responsive mb-5">
                <table class="table table-light table-borderless table-hover text-center mb-0">
                    <thead class="thead-dark">
                        <tr>
                            <th>ລໍາດັບ</th>
                            <th>ສິນຄ້່າ</th>
                            <th>ລາຍລະອຽດ</th>
                            <th>ລາຄາ</th>
                            <th>ຈຳນວນ</th>
                            <th>ເປັນເງິນ</th>
                            <th>ລຶບ</th>
                        </tr>
                    </thead>
                    @php
                        $num = 1;
                    @endphp
                    <tbody class="align-middle">
                        @if (count($shop_cart) > 0)
                            @foreach ($shop_cart as $item)
                                <tr>
                                    <td>{{ $num++ }}</td>
                                    <td class="align-middle"><img src="img/product-1.jpg" alt=""
                                            style="width: 50px;">
                                        {{ $item->name }}</td>
                                        <td>
                                            {{ $item->size }},
                                            {{ $item->color }}
                                        </td>
                                    <td class="align-middle"> {{ number_format($item->price) }} ₭</td>
                                    <td class="align-middle">
                                        <div class="input-group quantity mx-auto" style="width: 100px;">
                                            {{-- <div class="input-group-btn">
                                            <button class="btn btn-sm btn-primary btn-minus">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </div> --}}
                                            <input type="number" min="1"
                                                wire:model.live="qty.{{ $item->id }}"
                                                wire:change="UpdateStock({{ $item->id }})"
                                                class="form-control form-control-sm bg-secondary border-0 text-center"
                                                value="{{ $item->qty }}"> {{ $item->qty }}
                                            {{-- <div class="input-group-btn">
                                            <button class="btn btn-sm btn-primary btn-plus">
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </div> --}}
                                        </div>
                                    </td>
                                    <td class="align-middle">{{ number_format($item->price * $item->qty) }} ₭</td>
                                    <td class="align-middle"><button wire:click='Remove_Item({{ $item->id }})'
                                            class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">
                                    <span class="text-danger"><i class="fas fa-box-open"></i> ບໍ່ມີສິນຄ້າໃນກະຕ່າ
                                    </span><a href="{{ route('frontend.shop') }}"> ໄປທີ່ຮ້ານຄ້າ <i
                                            class="fas fa-arrow-right"></i></a>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="col-lg-4">
                {{-- <form class="mb-30" action="">
                    <div class="input-group">
                        <input type="text" class="form-control border-0 p-4" placeholder="Coupon Code">
                        <div class="input-group-append">
                            <button class="btn btn-primary">Apply Coupon</button>
                        </div>
                    </div>
                </form> --}}
                <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3"><i
                            class="fas fa-calculator"></i> ຄິດໄລ່ກະຕ່າສິນຄ້າ</span></h5>
                <div class="bg-light p-30 mb-5">
                    <div class="border-bottom pb-2">
                        <div class="d-flex justify-content-between mb-3">
                            <h6>ເປັນເງິນ</h6>
                            <h6>{{ number_format($sum_subtotal) }} ₭</h6>
                        </div>
                        <div class="d-flex justify-content-between">
                            <h6 class="font-weight-medium">ຄ່າຂົນສົ່ງ</h6>
                            <h6 class="font-weight-medium">Free</h6>
                        </div>
                    </div>
                    <div class="pt-2">
                        <div class="d-flex justify-content-between mt-2">
                            <h5>ຍອດລວມ</h5>
                            <h5>{{ number_format($sum_subtotal) }} ₭</h5>
                        </div>
                        @if ($count_shop_carts)
                            <a href="{{ route('frontend.CheckOut') }}"
                                class="btn btn-block btn-primary font-weight-bold my-3 py-3">ໄປທີ່ການຊຳລະ <i
                                    class="fas fa-arrow-right"></i></a>
                        @else
                            <button disabled class="btn btn-block btn-primary font-weight-bold my-3 py-3">ໄປທີ່ການຊຳລະ
                                <i class="fas fa-arrow-right"></i></button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Cart End -->
</div>
